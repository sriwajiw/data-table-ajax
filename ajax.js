$(document).ready(function(){
$.ajax({
'url': "http://dummy.restapiexample.com/api/v1/employees", //URL where the info is at 
'type': "GET", //get from the api to show
'success': function(response){
var columns=[
//defining the columns of the table
{
"title":"ID", // name we see on the table
"data":"id" //name on JSON file
},
{
"title":"Employee Name",
"data":"employee_name"
},
{
"title":"Employee Salary",
"data":"employee_salary"
},
{
"title":"Employee Age",
"data":"employee_age"
},
{
"title":"Employee Picture",
"data":"profile_image"
}
];
var data=jQuery.parseJSON(response);
//function call to store response from api and stored in variable data
var newJson = {};
// creating new dictioanry 
newJson.columns=columns;//assigning columns value to the columns of new dictionary
newJson.data= data;////assigning data value to the columns of new dictionary
console.log(newJson);
mytable = $('#emptable').DataTable({ 
// #emptable id of the table in html 
'data': newJson.data,// assigning value to object
'columns': newJson.columns
});
}
});
});